//
//  Location.h
//  MyLocations
//
//  Created by demo on 8/11/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Location : NSManagedObject

@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) UNKNOWN_TYPE longitude;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) UNKNOWN_TYPE locationDescription;
@property (nonatomic, retain) id category;
@property (nonatomic, retain) UNKNOWN_TYPE placemark;

@end
