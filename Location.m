//
//  Location.m
//  MyLocations
//
//  Created by demo on 8/11/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import "Location.h"


@implementation Location

@dynamic latitude;
@dynamic longitude;
@dynamic date;
@dynamic locationDescription;
@dynamic category;
@dynamic placemark;

@end
